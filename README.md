# Vending Machine

This app is running on Heroku at [blue-vending-machine-challenge](https://blue-vending-machine-challenge.herokuapp.com).

## Frontend

created by React

![image.png](./image_ref/image.png)

### Design

- Use a real vending machine design for use familiarity
- Product window can scroll up or down for easily to find product
- User can insert banknote/coin after confirm buying
- Banknote/Coin stock appear on the right of the vending machine
- Create, Update or Delete product by click these buttons

![image-6.png](./image_ref/image-6.png)![image-7.png](./image_ref/image-7.png)![image-8.png](./image_ref/image-8.png)

![image-4.png](./image_ref/image-4.png)

- Update bacnknote/coin stock

![image-9.png](./image_ref/image-9.png)

![image-5.png](./image_ref/image-5.png)

## Backend

created by FastAPI
- FastAPI is deployed on Heroku https://dashboard.heroku.com/apps/blue-vending-machine-challenge
- Github repository for Heroku https://github.com/disra/blue-vending-machine-fastapi

![image-1.png](./image_ref/image-1.png)

**Endpoint**

- `/` Root
- `/get-product`        Get all product 
- `/create-product`     Create new product requires product ID, Name, Price and Quantity
- `/update-product`     Update existing product requires product ID
- `/delete-product`     Delete existing product by product ID
- `/get-coinstocks`     Get all banknote and coin stock
- `/update-coinstocks`  Update amount of banknote and coin stock
- `/calculate`          Calculate banknote/coin stock and remaining changes in vending machine

## Deploy

After frontend and backend are ready.

- Use `npm run build` to create production folder

![image.png](./image_ref/build.png)

- Copy `static` folder to fastapi api folder. For this project copy into `api/app`
- Copy `public/assets` into `api/app/static`
- Copy `build/index.html` into `api/app/templates`
- Use `jinja2` to make our app appear
``` python
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

...

app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")

@app.get("/")
async def serve_spa(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})
```

then push this `app` folder to `Github` and deploy on Heroku by connect pushed repository.
