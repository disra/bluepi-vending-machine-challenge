import React from "react";
import App from "./App";
import { shallow, mount } from "enzyme";

import { AdjustProduct, Backdoor, VendingMachine } from "./components";
import Product from "./components/product";
import ConfirmBox from "./components/confirm-box";

describe("App", () => {
  it("renders without crashing", () => {
    shallow(<App />);
  });
});

describe("AdjustProduct", () => {
  it("renders without crashing", () => {
    shallow(<AdjustProduct />);
  });
});

describe("Backdoor", () => {
  it("renders without crashing", () => {
    shallow(<Backdoor />);
  });
});

describe("ConfirmBox", () => {
  it("renders without crashing", () => {
    shallow(<ConfirmBox />);
  });
});

describe("Product", () => {
  it("renders without crashing", () => {
    shallow(<Product />);
  });
});

describe("VendingMachine", () => {
  it("renders without crashing", () => {
    shallow(<VendingMachine />);
  });
});
