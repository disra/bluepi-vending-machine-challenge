import "./App.scss";
import Swal from "sweetalert2";
import { Container, Row, Col } from "react-bootstrap";
import { useCallback, useEffect, useState } from "react";
import {
  VendingMachine,
  Backdoor,
  AdjustProduct,
  AdjustCoinStock,
} from "./components/index";

function App() {
  const [products, setProducts] = useState([
    { id: 1, name: "Product1", price: 15, quantity: 10 },
  ]);
  const [coinStocks, setCoinStocks] = useState([
    { id: 1, types: "bank", value: 1000, amount: 10 },
  ]);

  const restructure = (data) => {
    return Object.keys(data).map((item, index) => {
      return { ...Object.values(data)[index], id: item };
    });
  };

  const getProducts = useCallback(() => {
    let option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    fetch("get-product", option)
      .then((res) => res.json())
      .then((data) => {
        let temp = restructure(data);
        setProducts(temp);
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: err,
          showConfirmButton: false,
          timer: 1000,
        });
      });
  }, []);

  const getCoinStocks = useCallback(() => {
    let option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    fetch("get-coinstocks", option)
      .then((res) => res.json())
      .then((data) => {
        let temp = restructure(data);
        setCoinStocks(temp);
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: err,
          showConfirmButton: false,
          timer: 1000,
        });
      });
  }, []);

  const updateStock = (id, product) => {
    let option = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(product),
    };
    fetch(`update-product/${id}`, option)
      .then((res) => res.json())
      .then((data) => {
        let temp = restructure(data);
        setProducts(temp);
      })
      .catch((err) => {
        alert(err);
      });
  };

  const updateCoinStock = (coinstocks) => {
    let temp = restructure(coinstocks);
    setCoinStocks(temp);
  };

  useEffect(() => {
    getProducts();
    getCoinStocks();
  }, [getProducts, getCoinStocks]);

  return (
    <div className="App">
      <section className="headers">
        <Container>
          <Row>
            <Col>
              <h1 className="headers__title">Blue vending machines</h1>
            </Col>
          </Row>
        </Container>
      </section>
      <section>
        <Container>
          <Row className="justify-content-center">
            <Col sm={12} lg={9} xl={8}>
              <VendingMachine
                products={products}
                coinStocks={coinStocks}
                updateStock={updateStock}
                updateCoinStock={updateCoinStock}
              />
            </Col>
            <Col sm={12} lg={3}>
              <Backdoor coinStocks={coinStocks}></Backdoor>
            </Col>
          </Row>
        </Container>
      </section>
      <section className="mb-5">
        <Container>
          <Row className="position-static justify-content-center">
            <Col md={8} lg={6} className="position-static">
              <AdjustProduct
                products={products}
                restructure={restructure}
                setProducts={setProducts}
              />
            </Col>
            <Col md={4} lg={3} xl={2} className="position-static">
              <AdjustCoinStock
                coinStocks={coinStocks}
                restructure={restructure}
                setCoinStocks={setCoinStocks}
              />
            </Col>
          </Row>
        </Container>
      </section>
    </div>
  );
}

export default App;
