import { useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";

const AdjustCoinStock = ({ coinStocks = [], restructure, setCoinStocks }) => {
  const [show, setShow] = useState(false);
  const [req, setReq] = useState([]);
  const [canSubmit, setCanSubmit] = useState(false);

  const submit = (e) => {
    e.preventDefault();
    let option = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({coins: req}),
    };
    fetch(`/update-coinstocks`, option)
      .then((res) => res.json())
      .then((data) => {
        if (data.Error !== undefined) {
          throw data.Error;
        }
        data = restructure(data);
        setCoinStocks(data);
        Swal.fire({
          icon: "success",
          title: "Successfully",
          showConfirmButton: false,
          timer: 1500,
        });
        cancel();
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: err,
          showConfirmButton: false,
          timer: 1500,
        });
      });
  };

  const cancel = () => {
    setReq([]);
    setShow(false);
  };

  const updateCoin = (value, id, mode = "") => {
    let temp = req.filter((coin) => coin.id !== id);
    let updateCoin = req.filter((coin) => coin.id === id)[0];
    let updatedCoinStock = [
      ...temp,
      { ...updateCoin, amount: updateCoin.amount + value },
    ];
    if (mode !== "") {
      updatedCoinStock = [...temp, { ...updateCoin, amount: value }];
    }
    updatedCoinStock.sort((a, b) => (b.id > a.id ? -1 : 1));
    setReq(updatedCoinStock);
  };

  const handleChange = (e, id) => {
    let value = e.target.value ? e.target.value : "";
    updateCoin(parseInt(value), id, "change");
  };

  const increase = (id) => {
    updateCoin(1, id);
  };

  const decrease = (id) => {
    updateCoin(-1, id);
  };

  useEffect(() => {
    setReq(coinStocks);
  }, [show, coinStocks]);

  useEffect(() => {
    setCanSubmit(req.reduce((can, coin) => can &= !isNaN(coin.amount), true));
  }, [req]);

  return (
    <div className="adjust-coin-stock">
      <Form
        onSubmit={submit}
        className={`adjust-coin-stock__form ${show ? "show" : ""}`}
      >
        <div className="from-title">Update Coin stock</div>
        {req.map((coin, coinId) => {
          return (
            <Form.Group
              className="form-group"
              key={coinId}
              controlId={`${coin.types}${coin.value}`}
            >
              <Form.Label>{coin.value}</Form.Label>
              <Form.Control
                value={isNaN(coin.amount) ? "" : coin.amount}
                placeholder={isNaN(coin.amount) ? "" : coin.amount}
                onChange={(e) => handleChange(e, coin.id)}
                type="number"
              />
              <div className="form-button-group mt-0">
                <Button
                  className="form-button"
                  onClick={() => increase(coin.id)}
                >
                  +
                </Button>
                <Button
                  className="form-button"
                  onClick={() => decrease(coin.id)}
                >
                  -
                </Button>
              </div>
            </Form.Group>
          );
        })}
        <Form.Group className="form-button-group">
          <Button disabled={!canSubmit} className="form-button" type="submit">
            Submit
          </Button>
          <Button className="form-button btn btn-danger" onClick={cancel}>
            Cancel
          </Button>
        </Form.Group>
      </Form>
      <Button
        className="adjust-coin__button btn btn-primary"
        onClick={() => setShow(true)}
      >
        Update Coin stock
      </Button>
    </div>
  );
};

export default AdjustCoinStock;
